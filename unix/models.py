from django.db import models


class Passwd(models.Model):
    uid = models.CharField(mex_length=32)
    password = models.TextField()
    uid_number = models.PositiveIntegerField()
    gid_number = models.PositiveIntegerField()
    gecos = models.TextField()
    home = models.TextField()
    shell = models.TextField()

class Group(models.Model):
    gid = models.CharField(max_length=32)
    password = models.TextField(null=True)
    gid_number = models.PositiveIntegerField()
    member_uids = models.ManyToManyField(Passwd, related_name='groups')

class Subuid(models.Model):
    uid = models.ForeignKey(Passwd, on_delete=models.CASCADE, related_name='subuids')
    subuid = models.PositiveIntegerField()
    subuid_count = models.PositiveIntegerField()

class Subgid(models.Model):
    uid = models.ForeignKey(Passwd, on_delete=models.CASCADE, related_name='subgids')
    subgid = models.PositiveIntegerField()
    subgid_count = models.PositiveIntegerField()
